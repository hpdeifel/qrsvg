qrsvg
=====

Generates QR codes as SVG files.

This is an almost literal copy of
[an example from diagrams-qrcode](https://github.com/prowdsponsor/diagrams-qrcode/blob/master/examples/using-haskell-qrencode.hs)
by Felipe Lessa, which is provided under the BSD3 license.

Usage
-----

    $ echo "Hello" | qrsvg > hello.svg

Installation
------------

1. Install the `qrencode` library via your OS' package manager

2. Install [`stack`](http://haskellstack.org)

3. `cd` into this directory and type

        stack --local-bin-path ~/bin install

    If stack tells you to run `stack setup`, do so.
	
`qrsvg` will be installed to `~/bin/`.
