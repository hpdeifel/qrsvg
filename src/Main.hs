{-# LANGUAGE PartialTypeSignatures, OverloadedStrings #-}
module Main where

import qualified Data.QRCode as QR
import qualified Diagrams.QRCode as QR
import Diagrams.Prelude
import Diagrams.Backend.SVG
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy.Char8 as L8
import Lucid.Svg (renderBS)

-- automatically determine version
version :: Maybe Int
version = Nothing

encodeLevel :: QR.QREncodeLevel
encodeLevel = QR.QR_ECLEVEL_M

encodeMode :: QR.QREncodeMode
encodeMode = QR.QR_MODE_EIGHT

main :: IO ()
main = do
  qr <- BS.getContents
        >>= (\x -> QR.encodeByteString x version encodeLevel encodeMode True)
        >>= return . QR.toMatrix

  let dia = scale 6 $ QR.stroke $ QR.pathMatrix  qr

  L8.putStrLn $ renderBS $ renderDia SVG (SVGOptions (mkWidth 250) Nothing "") dia
